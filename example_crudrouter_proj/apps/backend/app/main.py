from fastapi import FastAPI
import uvicorn

from fastapi.middleware.cors import CORSMiddleware

from core.config import settings, logging_settings
from core.logger import get_logger
# from core.database import create_db_and_tables

from src.routers import api_router

log = get_logger(__name__)
log.setLevel(logging_settings.LOG_LEVEL or "INFO")

print(f"Log Level: {logging_settings.LOG_LEVEL}")


def create_app() -> FastAPI:

    app = FastAPI(
        title=settings.APP_TITLE,
        description=settings.APP_DESCRIPTION,
        version=settings.APP_VERSION,
    )

    log.debug(f"App Title: {app.title}")
    log.debug(f"App Description: {app.description}")
    log.debug(f"App Version: {app.version}")

    app.include_router(api_router.router)

    return app


log.info("Creating FastAPI app")
app = create_app()


if settings.BACKEND_CORS_ORIGINS:
    log.info("Adding app middleware")
    log.debug(f"Origins: {[str(origin) for origin in settings.BACKEND_CORS_ORIGINS]}")

    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@app.on_event("startup")
async def startup_event():
    log.info("Starting app")
    # logger = get_uvicorn_logger()


@app.get("/")
async def root():

    log.debug("Root / reached")
    return {"message": f"Welcome to {settings.APP_TITLE}"}


if __name__ == "__main__":
    uvicorn.run("main:app", port=settings.APP_PORT, host=settings.APP_HOST, reload=True)
