# from fastapi_crudrouter import MemoryCRUDRouter as CRUDRouter
from fastapi_crudrouter import SQLAlchemyCRUDRouter

from core.config import settings, logging_settings
from core.logger import get_logger

log = get_logger(__name__)
log.setLevel(logging_settings.LOG_LEVEL)

from src.dependencies import get_db, create_db_and_tables

from src.models.example_models import PotatoCreate, Potato, PotatoModel, Base

tags = ["CRUD router example"]

router = SQLAlchemyCRUDRouter(
    schema=Potato,
    create_schema=PotatoCreate,
    db_model=PotatoModel,
    db=get_db,
    prefix="potato",
    tags=tags
)


@router.on_event("startup")
async def startup_event():
    log.info("Preparing database")
    create_db_and_tables(Base)
