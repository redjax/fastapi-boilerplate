from core.database import SessionLocal, engine


def get_db():
    
    db = SessionLocal()
    
    try:
        yield db
    finally:
        db.close()
        
        
def create_db_and_tables(base):
    
    base.metadata.create_all(bind=engine)
