from pydantic import BaseModel
from sqlalchemy import Column, String, Float, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class PotatoCreate(BaseModel):
    
    thickness: float
    mass: float
    color: str
    type: str


class Potato(PotatoCreate):
    
    id: int

    class Config:
        orm_mode = True
        
        
class PotatoModel(Base):
    
    __tablename__ = "potatoes"
    
    id = Column(Integer, primary_key=True, index=True)
    thickness = Column(Float)
    mass = Column(Float)
    color = Column(String)
    type = Column(String)
