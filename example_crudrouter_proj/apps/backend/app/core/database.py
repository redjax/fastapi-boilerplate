from pathlib import Path

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from core.config import settings, database_settings, logging_settings

from core.logger import get_logger
log = get_logger(__name__)
log.setLevel(logging_settings.LOG_LEVEL)


connect_args = {"check_same_thread": False}
engine = create_engine(database_settings.DB_URI, connect_args=connect_args, echo=True)


SessionLocal = sessionmaker(
    autocommit=False,
    autoflush=False,
    bind=engine
)
