## Notice

I am refactoring this project. The notes & instructions in this README may be out of date temporarily. I'll remove this notice when the project is properly documented.

**Table of Contents**

- [Description](#description)
- [Setup](#setup)
  - [Creating a new endpoint](#creating-a-new-endpoint)
- [Running](#running)
  - [Option 1: manage.sh script](#option-1-managesh-script)
  - [Option 2: Manual CLI launch](#option-2-manual-cli-launch)
  - [Option 3: Debug launch](#option-3-debug-launch)
- [Project Structure](#project-structure)
  - [Files](#files)
  - [Project Tree](#project-tree)
- [Helpful Notes/Links](#helpful-noteslinks)
  - [Notes](#notes)
    - [How to use url_for() to serve static files](#how-to-use-url_for-to-serve-static-files)
    - [How to create/use HTML forms](#how-to-createuse-html-forms)
  - [Links](#links)

# Description

I was finding the [officially documented FastAPI project structure](https://fastapi.tiangolo.com/tutorial/bigger-applications/) confusing, and I was not using it when I originally started learning FastAPI. Each time I started a project, I would cherry pick things and make small improvements, until I started gravitating to a more standardized setup.

- For more resources I used in developing this boilerplate, check the [Helpful Links/Notes](#helpful-noteslinks) section at the bottom of this README.

- To see the project structure at a glance, check the [project tree](#project-tree) section below.

- Check the [setup](#setup) section for instructions when cloning a fresh boilerplate project

Now, I begin my projects the same way, and they typically end up being structured the same. This repo is an example project that can be cloned, tweaked, and used to scaffold a new FastAPI project.

This boilerplate was built mostly by following the example laid out in the very useful [fastapi-nano](https://github.com/rednafi/fastapi-nano) repository from rednafi. I made a few adjustments (such as moving routing code out of the `api_router.py` and into each endpoint's own `*_router.py` file, which is then added to the main `api_router.py` file), tweaked the project layout and some filenames so they make more sense with the way I structure my projects.

# Setup

- Clone the repository
- (Optional) If you plan to commit your new project to git, remove the `.git` directory
  - (from the fastapi-boilerplate root directory):

    ```
    $ rm -r .git
    ```

- (Optional) Create the Conda env
    ```
    $ conda env create -f conda_envs/fastapi_boilerplate.yml
    ```
    - If you use [mamba](https://anaconda.org/conda-forge/mamba) run this instead:
      ```
      $ mamba env create -f conda_envs/fastapi_boilerplate.yml
      ```
- Create a `.env` file
  - Copy `app/core/conf/.env.example` to `app/core/conf/.env`
  - Edit the `APP_DESCRIPTION`, `APP_TITLE`, and `API_VERSION` veriables as needed.
  - When running live, don't forget to change `LOG_LEVEL` to `"info"` so you don't accidentally write debug messages in production.
- (Optional): After referencing the files to create your own endpoints/models/schemas/repository functions, delete the example endpoints/schemas/models files to start fresh
  - Delete:
    - `app/apis/api_1`
    - `app/apis/example_database`
    - `app/apis/test`
  - Edit:
    - `app/core/repositories.py`
      - Remove imports for deleted routers
        - `from app.apis.api_1 import api_1_router`
        - `from app.apis.example_database import db_example_router`
      - Remove/edit `router.include` statements from `app/apis/api_router.py` file
        - `router.include_router(api_1_router.router)`
        - `router.include_router(db_example_router.router)`
        - **NOTE**: As you create new APIs/endpoints, don't forget to add their router to `app/apis/api_router.py`'s `router.include_router()` statements
          - i.e. if you create a new API at `app/apis/weather`, add an include to `app/apis/api_router.py` like:
```
# api_router.py

# Import the weather router
from app.apis.weather import router

...

# Include the router
router.include_router(weather.router)

...
```

  - Edit/delete models & schemas
    - **NOTE**: You can opt to leave these files alone so you have a reference, but don't forget to remove the import statements in `app/core/repositories.py` & elsewhere
    - `app/models/test_models.py`
    - `app/models/person_models.py`
    - `app/schemas/test_schemas.py`
    - `app/schemas/person_schemas.py`
    - **NOTE**: Don't forget to remove import statements for these files from:
      - `app/core/repositories.py`
  - As you set up new endpoints, if you need to use database functions, reference the models, schemas, & repository functions to create your models/schemas/repository functions

## Creating a new endpoint

To add an endpoint to the API, create a new directory in `app/apis/new-endpoint`, and add the following common code before writing your own (the below code assumes no database is needed):

**NOTES**:
- In the LOGGING SETUP section, change the value of `logger_name` to either the name of the endpoint or the filename of the python file for the endpoint.
  - When a log is created, it adds the logger's name to the log entry to make debugging better, i.e.:
    ```
    [MM-DD-YYYY_HH:mm] [>> VALUE OF $logger_name HERE <<] LOGTYPE : LOG MESSAGE
    ```

```
# New endpoint common code

from fastapi import APIRouter
from app.core.config import settings

# >>>> START LOGGING SETUP
from app.core.log import logger

# Set name for logger
logger_name = "new_endpoint_name.py"

# Set the logger for this file
log = logger(logger_name)

# >>>> END LOGGING SETUP

# Import the error handler logger for unhandled exceptions throughout
from app.core.log import RouteErrorHandler as route_error_handler

# Create FastAPI router
router = APIRouter(
    route_class=route_error_handler,
    responses={
        404: {"error": "Not found"},
        500: {"server error": "Internal server error"},
    },
)

# Create endpoint for $new_endpoint_name root ("/NEW-ENDPOINT-PATH/")
@router.get("/NEW-ENDPOINT-PATH/")
async def index() -> dict[str, str]:
    """
    The -> arrow operator is used to annotate
    the return value for a function. It is not required
    and does not affect the program,
    but is useful as documentation, as it denotes
    what the return value will be.
    """

    log.debug("/ route reached")
    return {
        "info": {
            "message": "This is the index page for NEW-ENDPOINT-PATH.",
            "debug": "Docs: http://localhost:7001/docs",
        }
    }
```

This common code does a few things:

- Imports your settings from `.env` using `app/core/config.py`
- Sets up logging
  - Change the value of `logger_name` to identify logs for this file in the log output (CLI or file)
- Importes `RouteErrorHandler` class to handle unhandled exceptions in this file without crashing the app
- Creates a router
  - You don't need to edit the `APIRouter()` code unless you want to add to it
- Creates a response for requests to the new endpoint's root("/") path
  - This helps with testing up front to make sure you did everything you need to do, i.e. adding the router to `api_router.py`
  - After setting up your new endpoint and including the router in `api_router.py`, you should be able to send a request to `http://<HOST-ADDRESS>:<HOST-PORT>/api/v1/NEW-ENDPOINT-NAME/` and get a response indicating the request resolved successfully.
  - You can edit/remove this endpoint, or simply leave it and continue adding endpoints to the new API

# Running

There are 3 options for running the server.

## Option 1: manage.sh script

The `manage.sh` script can handle running the server for you. You can launch the server with: 

```
$ ./manage.sh -r
```

The default `$HOST` value is `0.0.0.0`, and the default `$PORT` value is `7001`. If you want to use a different port, edit `manage.sh`'s `export_vars()` function, changing the `export HOST` and/or `export PORT` values before running.

The script also creates an environment variable called `$TESTVAR`. This can be useful when testing environment values in the project; if you launch with the script, you can reference `os.getenv("TESTVAR")` in the script, which will load the `TESTVAR` variable from the environment.

## Option 2: Manual CLI launch

- Activate the Conda environment
    ```
    $ conda env activate fastapi_boilerplate
    ```
- Run the server, where `$host` is the host address you wish to use (set to `0.0.0.0` to access outside `localhost` while testing), and `$port` is the port you wish to use (default: 7001). **IMPORTANT**: If you change the port, edit the `.env` variable `APP_PORT` too
  ```
  $ uvicorn app.main:app --reload --host $host --port $port
  ```

## Option 3: Debug launch

There is an included `launch.json` file in the `.vscode` directory, which you can use to launch the server in a debugger. Note that if you do not want to use the default host of `0.0.0.0` or the default port of `7001`, you must change these variables in `launch.json`.

# Project Structure

Note: Check the [project tree](#project-tree) print below for a graphical representation of the repository, which should help with understanding the description below.

This file was built with larger/complex APIs in mind, but should be suited (if a little overkill) for smaller/simpler projects. The project is dynamic, with the core routing code living in `app/main.py` and `app/apis/api_router.py`. As you add endpoints to `app/apis/`, the only "core" file you need to update is `api_router.py` to include the new router with: `router.include_router(endpoint_name.router)`.

For example, if you create a new API at `app/apis/weather`, add an include to `app/apis/api_router.py` like:

```
# api_router.py

# Import the weather router
from app.apis.weather import router

...

# Include the router
router.include_router(weather.router)

...
```

## Files

- `pyproject.toml`
  - This project uses Poetry inside Conda
  - The `pyproject.toml` file describes the Poetry project
  - Edit this file's `[tool.poetry]` section to update your project's name, version, description, etc
- `app/`
- `conda_envs/`
  - Stores conda env(s)
  - The default `fastapi_boilerplate.yml` file has everything needed for the boilerplate. You can create your own conda_env if this file is not suited to your development needs
- `db/`
- `logs/`
- `app/main.py`
  - This is the "main" file which is run by the ASGI server (by default/for development, this is `uvicorn`)
  - You should not need to edit this file much, if at all
- `app/apis/api_router.py`
  - This is the "main" router
  - When you add endpoints to `app/apis/*`, you will add the endpoint's router to `api_router.py` to make it routable
  - The process to create a new endpoint is detailed in the [creating a new endpoint](#creating-a-new-endpoint) section.
- `app/core/conf`
  - Configuration-related files
  - Contains `config.py`, which handles looking up config values in your code
    - To import a configuration file from the environment/`.env` file, import `config.py` into a file and use dot notation to import the value, i.e.
    ```
    from app.core.config import settings

    # Access an environment value
    env_val = settings.VALUE_NAME

    # For example, to access the value of HOST
    #   host = settings.HOST
    ```
  - To add variables to the environment to access with the above method, add them to `app/core/conf/.env` and access the value by name with `settings.VAR_NAME`
- `app/core/db.py`
  - Handles database connections
  - Accessed in code by importing `from app.core.db import get_db, engine`, then adding to a request with `db: Session = Depends(get_db)`
- `app/core/log.py`
  - A custom logger written using Python's built-in `logging` library
    - To utilizie, check the [creating a new endpoint](#creating-a-new-endpoint) section, follow the example in the core code section for adding logging to an endpoint
    - You can use logging in non-endpoint code files too
- `app/core/repositories.py`
- `app/core/time_ops.py`
- `app/database`
- `app/models`
- `app/schemas`
- `app/tests`

## Project Tree

```
.
├── app
│   ├── apis
│   │   ├── api_1
│   │   │   ├── api_1_router.py
│   │   │   ├── mainmod.py
│   │   │   └── submod.py
│   │   ├── api_router.py
│   │   ├── example_database
│   │   │   ├── db_example_router.py
│   │   │   └── dbmod.py
│   │   ├── html_example
│   │   │   └── test_html_router.py
│   │   └── test
│   │       └── test.py
│   ├── core
│   │   ├── conf
│   │   ├── config.py
│   │   ├── db.py
│   │   ├── log.py
│   │   ├── repositories.py
│   │   └── time_ops.py
│   ├── database
│   │   ├── base_class.py
│   │   ├── base.py
│   │   ├── initialize.py
│   │   └── session.py
│   ├── main.py
│   ├── models
│   │   ├── person_models.py
│   │   └── test_models.py
│   ├── schemas
│   │   ├── person_schemas.py
│   │   ├── test_html_schemas.py
│   │   └── test_schemas.py
│   ├── static
│   │   └── img
│   │       └── gear-icon.png
│   ├── templates
│   │   ├── base.html
│   │   ├── live
│   │   │   ├── current_weather.html
│   │   │   └── modules
│   │   │       ├── head_imports.html
│   │   │       └── navbar.html
│   │   └── test
│   │       ├── modules
│   │       │   └── form.html
│   │       ├── test_form.html
│   │       ├── test_form_response.html
│   │       └── test.html
│   └── tests
│       ├── test_api.py
│       └── test_functions.py
├── conda_envs
│   ├── fastapi_boilerplate.yml
│   └── weatherapi_example.yml
├── example_projects
│   └── weatherapi
│       ├── app
│       │   ├── apis
│       │   │   ├── api_1
│       │   │   │   ├── api_1_router.py
│       │   │   │   ├── mainmod.py
│       │   │   │   └── submod.py
│       │   │   ├── api_router.py
│       │   │   ├── example_database
│       │   │   │   ├── db_example_router.py
│       │   │   │   └── dbmod.py
│       │   │   ├── test
│       │   │   │   └── test_router.py
│       │   │   └── weather
│       │   │       ├── weather_ops.py
│       │   │       └── weather_router.py
│       │   ├── core
│       │   │   ├── conf
│       │   │   ├── config.py
│       │   │   ├── db.py
│       │   │   ├── log.py
│       │   │   ├── repositories.py
│       │   │   └── time_ops.py
│       │   ├── database
│       │   │   ├── base_class.py
│       │   │   ├── base.py
│       │   │   ├── initialize.py
│       │   │   └── session.py
│       │   ├── main.py
│       │   ├── models
│       │   │   ├── person_models.py
│       │   │   ├── test_models.py
│       │   │   └── weather_models.py
│       │   ├── schemas
│       │   │   ├── person_schemas.py
│       │   │   ├── test_schemas.py
│       │   │   └── weather_schemas.py
│       │   ├── static
│       │   │   └── templates
│       │   │       ├── test
│       │   │       │   └── test.html
│       │   │       └── weather
│       │   │           └── current_weather.html
│       │   └── tests
│       │       ├── test_api.py
│       │       └── test_functions.py
│       ├── db
│       │   └── test.db
│       ├── manage.sh
│       └── README.md
├── manage.sh
├── poetry.lock
├── pyproject.toml
├── README.md
└── requirements.txt

39 directories, 73 files

```

# Helpful Notes/Links

## Notes

### How to use url_for() to serve static files

To serve files from the `static/` directory, you need to add a `mount` to your `app` in `main.py`. To utilize static files, you also need to import `StaticFiles` with `from fastapi.staticfiles import StaticFiles`.

Instructions:

1. Create a `mount` for the static files directory. Do this in your `main.py`
  ```
  # main.py

  ...
  from fastapi.staticfiles import StaticFiles

  ...
  
  app = FastAPI(title=title, description=description, version=version)

  # Mount static dir
  app.mount("/static", StaticFiles(directory="static"), name="static")

  ...
  ```

2. Use `{{ url_for('static', path="some/file.ext") }}` in Jinja templates to access resources in `static/`
   1. i.e. load an icon `gear-icon.png` from `static/img/`

  ```
  <!-- some_template.html -->

  <img src="{{ url_for('static', path='/img/gear-icon.png') }}" alt="" width="30" height="24">
  ```
  
  2. Or to load a CSS file from `static/css/`

  ```
  <!-- some_template.html -->

  <link rel="stylesheet" href="{{ url_for('static', path='css/styles.css') }}">
  ```

### How to create/use HTML forms

1. Ensure `python-multipart` is installed.
   1. This is included in the `fastapi_boilerplate` conda env included in the repo
      1. This is also included in `requirements.txt`
   2. If you're not using Conda and you're not installing dependencies with `requirements.txt`, install with `pip install python-multipart`
2. Create a schema to store Form response data
   1. `schemas/example_html_schemas.py`
  
      ```
      # schemas/example_html_schemas.py

      from fastapi import Form
      from pydantic import BaseModel

      class ExampleFormData(BaseModel):
          
          name: str
          age: int
        
          # Create class method to get data from HTML form
          @classmethod
          def as_form(cls, name: str = Form(...), age: int = Form(...)):
              
              # Return form data object
              return cls(name=name, age=age)
      ```

3. Create an HTML template with a form at `templates/live/example_form.html`
      
      ```
      <!-- templates/live/example_form.html -->
      
      {% extends 'base.html' %}
        
        {% block content %}

          <form method="post" id="exampleForm">
            <div class="mb-3">
              <label for="name" class="form-label">Name</label>
                <input
                  type="text"
                  class="form-control"
                  id="name"
                  name="name"
                  aria-describedby="nameHelp"
                />
              <div id="nameHelp" class="form-text">Your name, first or full name.</div>
            </div>
      
            <div class="mb-3">
              <label for="age" class="form-label">Age</label>
              <input type="number" class="form-control" id="age" name="age" />
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>

      {% endblock %}  
      ```
4. (Optional): Create a response HTML page to display form data at `templates/live/example_form_response.html`.
   1. Use `Jinja` templating to display form data values on the page, i.e. `{{ form_data.name }}`
   2. You must pass the form data in the `GET` request, covered below.

      ```
      <!-- templates/live/example_form_response.html -->

      {% extends 'base.html' %}

        {% block content %}

          <p>Name: {{ form_data.name }}</p>
          <p>Age: {{ form_data.age }}</p>

        {% endblock %}
      ```

5. Import FastAPI modules & do initial setup

      ```
      # apis/example_html/html_form.py

      # Import FastAPI modules
      from fastapi import Request
      from fastapi import Form, Depends
      from fastapi.responses import HTMLResponse
      from fastapi.staticfiles import StaticFiles
      from fastapi.templating import Jinja2Templates
      
      # Import example_html_schemas.py file
      from schemas import example_html_schemas

      # Create router object
      router = APIRouter(
        route_class=route_error_handler,
        responses={
          404: {"error": "Not found"},
          500: {"server_error: "Internal server error"}
        }
      )

      # Add templates directory to static mount
      #   Note: This assumes you have a line in your main.py to mount the static directory:
      #     app.mount("/static", StaticFiles(directory="static"), name="static")

      templates = Jinja2Templates(directory="templates")
      ``` 

6. Create 2 endpoints, a `GET` and a `POST`
   1. The `GET` endpoint will be used to render a form for the user to complete
   2. The `POST` endpoint will POST the data back to the server & create a class object of `ExampleFormData` with form's input


      ```
      # apis/example_html/html_form.py

      # GET endpoint to serve HTML form
      @router.get("/html_form/", response_class=HTMLResponse)
      async def render_form(request: Request) -> HTMLResponse:

        # Note: "active" below is used to set "active" tag on navbar links. It's not required
        return templates.TemplateResponse(
          "templates/live/example_form.html",
          {"request": request, "active": "example_form"}
        )

      # POST endpoint to submit form data to server
      @router.post(path="/html_form/", response_class=HTMLResponse)
      async def post_example_form(
        request: Request,
        form_data: example_html_schemas.ExampleFormData = Depends(
          example_html_schemas.ExampleFormData.as_form
        )
      ):

      # Pass form_data in response, access in template with {{ form_data.key }}
      return templates.TemplateResponse(
        "templates/live/example_form_response.html",
        {
          "request": request,
          "active": "example_form",
          "form_data": form_data
        }
      )
      ```

7. When a user navigates to `/html_form/`, they will be presented with the `templates/live/example_form.html` form. When the user fills & submits the form, they will get the `templates/live/example_form_response.html` response HTML, containing data from the submitted form.

---

## Links

- [rednafi/fastapi-nano Github Boilerplate](https://github.com/rednafi/fastapi-nano)
  - Used to set up this boilerplate
- [Better/newer FastAPI Boilerplate Github Repo](https://github.com/teamhide/fastapi-boilerplate)
- [FastAPI Boilerplate Github Repo](https://github.com/skb1129/fastapi-boilerplate)
- [Awesome FastAPI List](https://project-awesome.org/mjhea0/awesome-fastapi)
- [Python -> Arrow Operator Explained](https://pencilprogrammer.com/python-arrow-annotation/#:~:text=Conclusion,as%20documentation%20for%20the%20function.)
  - [Another good (maybe better?) writeup on arrow notation in Python](https://medium.com/@thomas_k_r/whats-this-weird-arrow-notation-in-python-53d9e293113)
- [Building a Python Logging Module for your Apps](https://python.plainenglish.io/building-python-logging-module-for-your-applications-54b7b0a93250)
  - Really helpful guide on building a custom logger
- [Youtube: incompetant_ian: Forms & File Uploads with FastAPI & Jinja2](https://www.youtube.com/watch?v=L4WBFRQB7Lk)
- [Reference for a sticky footer with Bootstrap](https://getbootstrap.com/docs/5.0/examples/sticky-footer-navbar/)
