from typing import Optional, List

from pydantic import condecimal
from sqlmodel import DateTime, Field, SQLModel, Relationship, DATETIME

 
billingStatusVals = []
billingFrequencyVals = ["Daily", "Weekly", "Monthly", "Yearly"]


class BillingStatus(SQLModel, table=True):
    
    __tablename__ = "sqlmodelBillingStatus"
    
    id: Optional[int] = Field(default=None, primary_key=True)
    statusName: str = ""
    
    
class BillingFrequency(SQLModel, table=True):
    
    __tablename__ = "sqlmodelBillingFrequency"
    
    id: Optional[int] = Field(default=None, primary_key=True)
    frequencyName: str = ""
    
    
class PaymentMethod(SQLModel, table=True):
    
    __tablename__ = "sqlmodelPaymentMethod"
    
    id: Optional[int] = Field(default=None, primary_key=True)
    paymentMethodType: str = ""
    paymentMethodAssocBank: Optional[str] = ""
    paymentMethodExpiration: Optional[str] = ""


class Subscription(SQLModel, table=True):
    
    __tablename__ = "sqlmodelSubscription"
    
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str = Field(index=True)
    description: Optional[str] = Field(default=None)
    price: Optional[condecimal(max_digits=4, decimal_places=2)] = Field(default=None)
    paymentMethod: Optional[int] = Field(default=None, index=True, foreign_key="sqlmodelPaymentMethod.id")
    billingStatusCode: Optional[int] = Field(default=None, index=True, foreign_key="sqlmodelBillingStatus.id")
    billingFrequencyCode: Optional[int] = Field(default=None, index=True, foreign_key="sqlmodelBillingFrequency.id")
    notes: Optional[str] = Field(default=None)
