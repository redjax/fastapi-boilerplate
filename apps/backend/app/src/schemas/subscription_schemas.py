from pydantic import BaseModel

class SubscriptionBase(BaseModel):
    
    name: str
    description: str | None = None
    price: float = 0.00
    

class SubscriptionCreate(SubscriptionBase):
    
    pass


class SubscriptionUpdate(SubscriptionBase):
    
    pass

class SubscriptionDelete(SubscriptionBase):
    
    pass


class Subscription(SubscriptionBase):
    
    id: int
    
    class Config:
        orm_mode = True
