from fastapi import APIRouter

from src.routers.example_database import example_db_router
from src.routers.subscriptions import subscriptions_router
from src.routers.sqlmodel_test import sqlmodel_router

from core.config import settings, logging_settings
from core.logger import get_logger

log = get_logger(__name__)
log.setLevel(logging_settings.LOG_LEVEL)

prefix = "/api/v1"
log.debug(f"Route prefix: {prefix}")

router = APIRouter(
    # route_class=route_error_handler,
    prefix=prefix,
    responses={
        404: {"error": "Not found"},
        500: {"server_error": "Internal server error"}
    }
)

router.include_router(example_db_router.router)
router.include_router(subscriptions_router.router)
router.include_router(sqlmodel_router.router)

