from fastapi import APIRouter

from core.config import settings, logging_settings, database_settings

from core.logger import get_logger

## Database
from fastapi import Depends, HTTPException
# from src.routers.example_database import example_db_crud
from src.models import subscription_models
from src.schemas import subscription_schemas
from . import subscriptions_crud
from core.crud import SubscriptionCrud
from core.database import SessionLocal, engine
from src.dependencies import get_db
from sqlalchemy.orm import Session
from typing import List

log = get_logger(__name__)
log.setLevel(logging_settings.LOG_LEVEL)

tags = ["subscriptions"]


router = APIRouter(
    # route_class=route_error_handler,
    responses={
        404: {"error": "Not found"},
        500: {"server_error": "Internal server error"}
    }    
)

# subscription_models.Base.metadata.create_all(bind=engine)

log.debug("Getting DB connection")
get_db()


@router.post("/subscriptions/", response_model=subscription_schemas.Subscription, tags=[tags])
def create_subscription(subscription: subscription_schemas.SubscriptionCreate, db: Session = Depends(get_db)):
    
    db_subscription = subscriptions_crud.get_by_name(db, subscription_name=subscription.name)
    
    if db_subscription:
        raise HTTPException(status_code=400, detail=f"Subscription already exists: {subscription.name}")
    
    return subscriptions_crud.create(db=db, subscription=subscription)


@router.get("/subscriptions/all", response_model=List[subscription_schemas.Subscription], tags=[tags])
def get_subscriptions(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    
    # subscriptions = subscriptions_crud.get_all(db)
    subscriptions = subscriptions_crud.get_all(db)
    
    # subscriptions = []
    
    # db_subscriptions = subscriptions_crud.get_all(db)
    # subscriptions.append(db_subscriptions)
    
    return subscriptions


@router.get("/subscriptions/id/{subscription_id}", response_model=subscription_schemas.Subscription, tags=[tags])
def get_subscription_by_id(subscription_id: int, db: Session = Depends(get_db)):
    
    subscription = subscriptions_crud.get_by_id(db, subscription_id)
    
    return subscription


# @router.get("/subscriptions/name/{subscription_name}", response_model=subscription_schemas.Subscription, tags=[tags])
# def get_subscription_by_name(subscription_name: str, db: Session = Depends(get_db)):
    
#     subscription = subscriptions_crud.get_by_name(db, subscription_name)

# @router.put("/subscriptions/id/{subscription_id}")
# def update_subscription(subscription_id: int, subscription: subscription_schemas.SubscriptionUpdate, db: Session = Depends(get_db)):
    
#     db_subscription = 

@router.delete("/subscriptions/{subscription_id}", tags=[tags])
def delete_subscription(subscription_id: int, db: Session = Depends(get_db)):
    
    subscription = subscriptions_crud.get_by_id(db, subscription_id)

    if subscription is None:
        raise HTTPException(status_code=404, detail=f"No subscription found with ID: {subscription_id}")

    subscriptions_crud.delete_subscription(db, subscription_id)
    
    return {"success": f"Deleted subscription with ID: {subscription.id}"}
