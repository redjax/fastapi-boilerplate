from http.client import HTTPException
from sqlalchemy.orm import Session

from src.models import subscription_models
from src.schemas import subscription_schemas


def get_by_id(db: Session, subscription_id: int):
    
    return db.query(subscription_models.Subscription).filter(subscription_models.Subscription.id == subscription_id).first()


def get_by_name(db: Session, subscription_name: str):
    
    return db.query(subscription_models.Subscription).filter(subscription_models.Subscription.name == subscription_name).first()


def get_all(db: Session, skip: int = 0, limit: int = 100):
    
    return db.query(subscription_models.Subscription).offset(skip).limit(limit).all()


def create(db: Session, subscription: subscription_schemas.SubscriptionCreate):
    
    db_subscription = subscription_models.Subscription(name=subscription.name, description=subscription.description, price=subscription.price)
    
    db.add(db_subscription)
    db.commit()
    db.refresh(db_subscription)
    
    return db_subscription


# def update(db: Session, subscription: subscription_schemas.SubscriptionUpdate):
    
#     db_subscription = subscription_models.Subscription()


def delete_subscription(db: Session, subscription_id: int):
    
    db_subscription = db.query(subscription_models.Subscription).filter(subscription_models.Subscription.id == subscription_id).first()
        
    if db_subscription:
        db.delete(db_subscription)
        db.commit()
        db.close()
        
        return {"success": f"Deleted subscription with ID: {subscription_id}"}
    else:
        raise HTTPException(status_code=404, detail=f"Subscription with ID: {subscription_id} not found.")
    # finally:
    #     return {"success": f"Deleted subscription with ID: {subscription_id}"}
