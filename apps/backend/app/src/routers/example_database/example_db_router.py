from fastapi import APIRouter

from core.config import settings, logging_settings, database_settings

from core.logger import get_logger

## Database
from fastapi import Depends, HTTPException
from src.routers.example_database import example_db_crud
from src.models import example_models
from src.schemas import example_schemas
from core.database import SessionLocal, engine
from src.dependencies import get_db
from sqlalchemy.orm import Session
from typing import List

log = get_logger(__name__)
log.setLevel(logging_settings.LOG_LEVEL)


tags = ["example"]

router = APIRouter(
    # route_class=route_error_handler,
    responses={
        404: {"error": "Not found"},
        500: {"server_error": "Internal server error"}
    }    
)

# example_models.Base.metadata.create_all(bind=engine)

log.debug("Getting DB connection")
get_db()

@router.post("/users/", response_model=example_schemas.User, tags=[tags])
def create_user(user: example_schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = example_db_crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return example_db_crud.create_user(db=db, user=user)


@router.get("/users/", response_model=List[example_schemas.User], tags=[tags])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = example_db_crud.get_users(db, skip=skip, limit=limit)
    return users


@router.get("/users/{user_id}", response_model=example_schemas.User, tags=[tags])
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = example_db_crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@router.post("/users/{user_id}/items/", response_model=example_schemas.Item, tags=[tags])
def create_item_for_user(
    user_id: int, item: example_schemas.ItemCreate, db: Session = Depends(get_db)
):
    return example_db_crud.create_user_item(db=db, item=item, user_id=user_id)


@router.get("/items/", response_model=List[example_schemas.Item], tags=[tags])
def read_items(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    items = example_db_crud.get_items(db, skip=skip, limit=limit)
    return items
