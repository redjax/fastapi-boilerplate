from fastapi import APIRouter

from core.config import settings, logging_settings, database_settings
from core.sqlmodel_database import engine, create_db_and_tables

from core.logger import get_logger

log = get_logger(__name__)
log.setLevel(logging_settings.LOG_LEVEL)

## Database
create_db_and_tables()


tags = ["sqlmodel", "test"]

router = APIRouter(
    # route_class=route_error_handler,
    responses={
        404: {"error": "Not found"},
        500: {"server_error": "Internal server error"}
    }    
)

@router.get("/sqlmodel-test/", tags=tags)
async def sqlmodel_root():
    
    return {"message": "sqlmodel-test route reached"}
