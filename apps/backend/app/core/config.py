from typing import Union
from pydantic import BaseSettings, Field, AnyHttpUrl

class Settings(BaseSettings):
    BACKEND_CORS_ORIGINS: list[Union[str, AnyHttpUrl]] = ["http://localhost:8000"]
    TEST: str = "This was set in config.py"
    
    # Prefix for endpoints, i.e. example.com/api/v1/endpoint
    ROUTE_PREFIX: str = "/api/v1"
    APP_TITLE: str = "FastAPI - Boilerplate"
    APP_DESCRIPTION: str = "FastAPI, Docker, NGINX, & more"
    APP_VERSION: str = "0.1"
    APP_HOST: str = "0.0.0.0"
    APP_PORT: int = 7001

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        case_sensitive = True
        

class LoggingSettings(Settings):
    
    LOG_LEVEL: str = "info"
    LOG_FILE: str = ""
    
    class Config:
        env_file = ".logging.env"
        env_file_encoding = "utf-8"
        case_sensitive = True
        
        
class DatabaseSettings(Settings):
    
    DB_HOST: str = None
    DB_PORT: int = 5432
    DB_USER: str = None
    DB_PASSWORD: str = None


settings = Settings()
logging_settings = LoggingSettings()
database_settings = DatabaseSettings()
