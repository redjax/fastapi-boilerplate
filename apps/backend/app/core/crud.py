from sqlalchemy.orm import Session
from src.models import subscription_models, example_models
from src.schemas import subscription_schemas, example_schemas

import json
from core.config import logging_settings
from core.logger import get_logger

log = get_logger(__name__)
log.setLevel(logging_settings.LOG_LEVEL)


class SubscriptionCrud:
    
    async def create(db: Session, subscription: subscription_schemas.SubscriptionCreate):
        
        db_subscription = subscription_models.Subscription(
            name=subscription.name,
            description=subscription.description,
            price=subscription.price
        )
        
        db.add(db_subscription)
        db.commit()
        db.refresh(db_subscription)
        
        return db_subscription
    
    
    async def get_by_id(db: Session, subscription_id: int):
    
        return db.query(subscription_models.Subscription).filter(subscription_models.Subscription.id == subscription_id).first()


    async def get_by_name(db: Session, subscription_name: str):
        
        return db.query(subscription_models.Subscription).filter(subscription_models.Subscription.name == subscription_name).first()


    async def get_all(db: Session, skip: int = 0, limit: int = 100):
        
        subscriptions = db.query(subscription_models.Subscription).offset(skip).limit(limit).all()
        
        log.debug(f"Subscriptions: {subscriptions}")
        return await subscriptions
