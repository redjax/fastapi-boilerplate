"""
Extensions for logger.py.
"""
## FastAPI logging
from pydantic import BaseModel
from fastapi.routing import APIRoute
from typing import Callable
from fastapi import Request, Response, HTTPException
import logging


class RouteErrorHandler(APIRoute):
    
    """https://stackoverflow.com/a/69720977

    Class to handle all uncaught exceptions.

    Note: Depends on fastapi package. Comment/remove
    this class and the import for RouteErrorHandler class
    at top of file.

    If logging to a file, this class will log the full Traceback
    message, which may span multiple lines. It looks like a standard
    sys.stdout message
    """

    def get_route_handler(self) -> Callable:
        
        original_route_handler = super().get_route_handler()
        
        async def custom_route_handler(request: Request) -> Response:
            
            try:
                return await original_route_handler(request)
            except Exception as ex:
                if isinstance(ex, HTTPException):
                    raise ex
                
                raise HTTPException(status_code=500, detail=str(ex))
            
        return custom_route_handler

