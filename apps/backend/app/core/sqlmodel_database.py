from sqlmodel import SQLModel, create_engine, Session, select, or_
from core.database import SQLALCHEMY_DATABASE_URL

from src.models.subscription_sqlmodel import BillingStatus, Subscription

from core.config import database_settings, logging_settings
from core.logger import get_logger

log = get_logger(__name__)
log.setLevel(logging_settings.LOG_LEVEL)


SQLALCHEMY_DATABASE_URL = f"postgresql://{database_settings.DB_USER}:{database_settings.DB_PASSWORD}@{database_settings.DB_HOST}/test"

log.debug(f"Database connection string: {SQLALCHEMY_DATABASE_URL}")

log.info("Creating DB engine")

engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=True)


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)
